#!/bin/bash

trap 'notify-send --app-name Backup "Error while executing: $BASH_COMMAND. Exiting..."; exit 1' ERR

notify-send --app-name Backup "Starting new backup..."

SOURCE=$HOME/                       # Local directory to be backed up
SERVER="tom@storage.org"            # Remote SSH server (with username) to be used for the backup
BACKUP="/mnt/backup/laptop/tom"     # Remote backup directory on the server

OPTIONS="--archive --one-file-system --hard-links --xattrs --exclude-from=$HOME/backup.exclude --fuzzy --link-dest=../latest --compress --bwlimit=280K --partial"

ionice -c2 -n7 nice -n 19 rsync $OPTIONS "$SOURCE" "$SERVER:$BACKUP/latest.tmp"

ssh $SERVER "mv '$BACKUP/latest' \"$BACKUP/backup \$(date -r $BACKUP/latest '+%F %T')\""
ssh $SERVER "mv '$BACKUP/latest.tmp' '$BACKUP/latest'"
ssh $SERVER "touch '$BACKUP/latest'"     # Timestamp the backup

notify-send --app-name Backup "... Backup script completed"