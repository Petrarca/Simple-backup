# Simple-backup

A very simple backup script using rsync over ssh. It can be run directly from the terminal or more comfortable via ulauncher.
A few comments:
* Obviously, a few things need to be adopted (e.g. directory to backup, server, user, bandwith-limit, excluded files)
* The first run might take *very long* as this will be a full backup
* Any subsequent backups will be much faster as only differences will be uploaded
* On the backup server hard-links will be used, i.e. identical files will be stored only once one the server